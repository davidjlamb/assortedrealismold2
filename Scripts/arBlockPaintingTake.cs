using System;
using UnityEngine;
using System.Collections;
using System.Globalization;
using GUI_2;

public class BlockPaintingTake : Block
{
    private float fTakeDelay = 15f;

    public override void Init()
    {
        base.Init();
        if (this.Properties.Values.ContainsKey("TakeDelay"))
            this.fTakeDelay = StringParsers.ParseFloat(this.Properties.Values["TakeDelay"], 0, -1, NumberStyles.Any);
    }

    public override string GetActivationText(global::WorldBase _world, global::BlockValue _blockValue, int _clrIdx, global::Vector3i _blockPos, global::EntityAlive _entityFocusing)
    {
        PlayerActionsLocal playerInput = ((EntityPlayerLocal)_entityFocusing).playerInput;
        string keybindString = UIUtils.GetKeybindString(playerInput.Activate, playerInput.PermanentActions.Activate);
        string str1 = _blockValue.Block.PickedUpItemValue == null ? Localization.Get(Block.list[_blockValue.type].GetBlockName(), "") : Localization.Get(_blockValue.Block.PickedUpItemValue, "");
        return string.Format(Localization.Get("lootItem", ""), (object)keybindString, (object)str1);
    }

    public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        if (_indexInBlockActivationCommands != 0)
            return false;
        LocalPlayerUI playerUi = (_player as EntityPlayerLocal).PlayerUI;
        playerUi.windowManager.Open("timer", true, false, true);
        XUiC_Timer childByType = playerUi.xui.GetChildByType<XUiC_Timer>();
        TimerEventData _eventData = new TimerEventData();
        _eventData.CloseEvent += this.EventData_CloseEvent;
        _eventData.Data = (object)new object[4]
        {
          (object) _cIdx,
          (object) _blockValue,
          (object) _blockPos,
          (object) _player
        };
        _eventData.Event += this.EventData_Event;
        childByType.SetTimer(this.fTakeDelay, _eventData);
        return true;
    }

    private void EventData_CloseEvent(TimerEventData timerData)
    {
        this.ResetEventData(timerData);
    }

    private void EventData_Event(TimerEventData timerData)
    {
        World world = GameManager.Instance.World;
        object[] array = (object[])timerData.Data;
        int clrIdx = (int)array[0];
        BlockValue blockValue = (BlockValue)array[1];
        Vector3i vector3i = (Vector3i)array[2];
        BlockValue block = world.GetBlock(vector3i);
        EntityPlayerLocal entityPlayerLocal = array[3] as EntityPlayerLocal;
        LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);

        ItemValue itemValue = ItemClass.GetItem(this.PickedUpItemValue, false);
        ItemStack itemStack = new ItemStack(itemValue, 1);
        if (!uiforPlayer.xui.PlayerInventory.AddItem(itemStack, true))
            uiforPlayer.xui.PlayerInventory.DropItem(itemStack);
        BlockValue _blockValue = BlockPlaceholderMap.Instance.Replace(this.DowngradeBlock, world.GetGameRandom(), vector3i.x, vector3i.z, false, QuestTags.none);
        _blockValue.rotation = block.rotation;
        _blockValue.meta = block.meta;
        world.SetBlockRPC(clrIdx, vector3i, _blockValue, Block.list[_blockValue.type].Density);
        this.ResetEventData(timerData);
    }

    private void ResetEventData(TimerEventData timerData)
    {
        timerData.CloseEvent -= this.EventData_CloseEvent;
        timerData.Event -= this.EventData_Event;
    }
}