using System;
using UnityEngine;
using System.Collections;
using System.Globalization;
using GUI_2;

public class BlockLootTake : BlockLoot
{
    private float fTakeDelay = 45f;
    private BlockActivationCommand[] cmds;

    public BlockLootTake()
    {
        this.cmds = new BlockActivationCommand[2]
        {
            new BlockActivationCommand("Search", "search", true),
            new BlockActivationCommand("take", "hand", true)
        };
        this.HasTileEntity = true;
    }

    public override void Init()
    {
        base.Init();
        if (this.Properties.Values.ContainsKey("TakeDelay"))
            this.fTakeDelay = StringParsers.ParseFloat(this.Properties.Values["TakeDelay"], 0, -1, NumberStyles.Any);
    }

    public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
    {
        //TileEntitySecureLootContainer tileEntity = _world.GetTileEntity(_clrIdx, _blockPos) as TileEntitySecureLootContainer;
        //if (tileEntity == null)
        //    return new BlockActivationCommand[0];
        //string _steamID = GamePrefs.GetString(EnumGamePrefs.PlayerId);
        //PersistentPlayerData playerData = _world.GetGameManager().GetPersistentPlayerList().GetPlayerData(tileEntity.GetOwner());
        //bool flag = !tileEntity.IsOwner(_steamID) && (playerData != null && playerData.ACL != null && playerData.ACL.Contains(_steamID));
        //this.cmds[1].enabled = !tileEntity.IsLocked() && playerData == null;
        return this.cmds;
    }

    public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        if (_indexInBlockActivationCommands == 0)
            return this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
        if (_indexInBlockActivationCommands != 1)
            return false;
        LocalPlayerUI playerUi = (_player as EntityPlayerLocal).PlayerUI;
        playerUi.windowManager.Open("timer", true, false, true);
        XUiC_Timer childByType = playerUi.xui.GetChildByType<XUiC_Timer>();
        TimerEventData _eventData = new TimerEventData();
        _eventData.CloseEvent += this.EventData_CloseEvent;
        _eventData.Data = (object)new object[4]
        {
          (object) _cIdx,
          (object) _blockValue,
          (object) _blockPos,
          (object) _player
        };
        _eventData.Event += this.EventData_Event;
        childByType.SetTimer(this.fTakeDelay, _eventData);
        return true;
    }
    
    private void EventData_CloseEvent(TimerEventData timerData)
    {
        this.ResetEventData(timerData);
    }

    private void EventData_Event(TimerEventData timerData)
    {
        World world = GameManager.Instance.World;
        object[] array = (object[])timerData.Data;
        int clrIdx = (int)array[0];
        BlockValue blockValue = (BlockValue)array[1];
        Vector3i vector3i = (Vector3i)array[2];
        BlockValue block = world.GetBlock(vector3i);
        EntityPlayerLocal entityPlayerLocal = array[3] as EntityPlayerLocal;
        LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
        BlockValue pickUpBlock = (this.PickedUpItemValue == null ? blockValue : Block.GetBlockValue(this.PickedUpItemValue, true));
        ItemStack itemStack = new ItemStack(pickUpBlock.ToItemValue(), 1);
        if (!uiforPlayer.xui.PlayerInventory.AddItem(itemStack, true))
            uiforPlayer.xui.PlayerInventory.DropItem(itemStack);
        BlockValue downgradeBlock = BlockPlaceholderMap.Instance.Replace((this.DowngradeBlock.type != BlockValue.Air.type ? this.DowngradeBlock : BlockValue.Air), world.GetGameRandom(), vector3i.x, vector3i.z, false, QuestTags.none);
        downgradeBlock.rotation = block.rotation;
        downgradeBlock.meta = block.meta;
        world.SetBlockRPC(clrIdx, vector3i, downgradeBlock, Block.list[downgradeBlock.type].Density);
        this.ResetEventData(timerData);
    }

    private void ResetEventData(TimerEventData timerData)
    {
        timerData.CloseEvent -= this.EventData_CloseEvent;
        timerData.Event -= this.EventData_Event;
    }
}