using Audio;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Collections;
using System.Globalization;
using GUI_2;

public class BlockCarExplodeLootRepair : BlockCarExplodeLoot
{
    private float repairTime = 15f;
    private float repairChance = 0.45f;
    private float repairExpValue = 100f;
    private string repairedItemValue;
    private BlockActivationCommand[] cmds;

    public BlockCarExplodeLootRepair()
    {
        this.cmds = new BlockActivationCommand[2]
        {
            new BlockActivationCommand("Search", "search", true),
            new BlockActivationCommand("repair", "wrench", false)
        };
        this.HasTileEntity = true;
    }

    public override void Init()
    {
        base.Init();
        if (this.Properties.Values.ContainsKey("RepairTime"))
            this.repairTime = StringParsers.ParseFloat(this.Properties.Values["RepairTime"], 0, -1, NumberStyles.Any);
        if (this.Properties.Values.ContainsKey("RepairChance"))
            this.repairChance = StringParsers.ParseFloat(this.Properties.Values["RepairChance"], 0, -1, NumberStyles.Any);
        if (this.Properties.Values.ContainsKey("RepairExp"))
            this.repairExpValue = StringParsers.ParseFloat(this.Properties.Values["RepairExp"], 0, -1, NumberStyles.Any);
        if (this.Properties.Values.ContainsKey("RepairedItem"))
            this.repairedItemValue = this.Properties.Values["RepairedItem"];
    }

    public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _player)
    {

        if (_player.inventory.holdingItem.Name == "meleeToolWrench" || _player.inventory.holdingItem.Name == "meleeToolClawHammer")
            this.cmds[1].enabled = true;
        else
            this.cmds[1].enabled = false;
        if (this.repairedItemValue == "")
            this.cmds[1].enabled = false;
        return this.cmds;
    }

    public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        if (_indexInBlockActivationCommands == 0)
            return this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
        if (_indexInBlockActivationCommands != 1)
            return false;
        LocalPlayerUI playerUi = (_player as EntityPlayerLocal).PlayerUI;
        ItemValue itemValueClawHammer = ItemClass.GetItem("meleeToolClawHammer", false);
        if (playerUi.xui.PlayerInventory.GetItemCount(itemValueClawHammer) == 0)
        {
            playerUi.xui.CollectedItemList.AddItemStack(new ItemStack(itemValueClawHammer, 0), true);
            GameManager.ShowTooltip(_player as EntityPlayerLocal, Localization.Get("ttMissingCraftingResources", ""));
            return true;
        }
        ItemValue itemValueWireTool = ItemClass.GetItem("meleeToolWireTool", false);
        if (playerUi.xui.PlayerInventory.GetItemCount(itemValueWireTool) == 0)
        {
            playerUi.xui.CollectedItemList.AddItemStack(new ItemStack(itemValueWireTool, 0), true);
            GameManager.ShowTooltip(_player as EntityPlayerLocal, Localization.Get("ttMissingCraftingResources", ""));
            return true;
        }
        ItemValue itemValueWrench = ItemClass.GetItem("meleeToolWrench", false);
        if (playerUi.xui.PlayerInventory.GetItemCount(itemValueWrench) == 0)
        {
            playerUi.xui.CollectedItemList.AddItemStack(new ItemStack(itemValueWrench, 0), true);
            GameManager.ShowTooltip(_player as EntityPlayerLocal, Localization.Get("ttMissingCraftingResources", ""));
            return true;
        }
        float repairDifficulty = UnityEngine.Random.Range(-1f, 4f);
        foreach (Block.SItemNameCount sItemNameCount in this.RepairItems)
        {
            ItemValue itemValue = ItemClass.GetItem(sItemNameCount.ItemName, false);
            int count = (int)Math.Round(sItemNameCount.Count * repairDifficulty, 0);
            if (sItemNameCount.ItemName == "smallEngine" && count > 1)
                count = 1;
            if (sItemNameCount.ItemName == "carBattery" && count > 1)
                count = 1;
            if (sItemNameCount.ItemName == "resourceRadiator" && count > 1)
                count = 1;
            if (playerUi.xui.PlayerInventory.GetItemCount(itemValue) < count)
            {
                playerUi.xui.CollectedItemList.AddItemStack(new ItemStack(itemValue, 0), true);
                GameManager.ShowTooltip(_player as EntityPlayerLocal, Localization.Get("ttMissingCraftingResources", ""));
                return true;
            }
        }
        foreach (Block.SItemNameCount sItemNameCount in this.RepairItems)
        {
            ItemValue itemValue = ItemClass.GetItem(sItemNameCount.ItemName, false);
            int count = (int)Math.Round(sItemNameCount.Count * repairDifficulty, 0);
            if (sItemNameCount.ItemName == "smallEngine" && count > 1)
                count = 1;
            if (sItemNameCount.ItemName == "carBattery" && count > 1)
                count = 1;
            if (sItemNameCount.ItemName == "resourceRadiator" && count > 1)
                count = 1;
            playerUi.xui.PlayerInventory.RemoveItem(new ItemStack(itemValue, count));
        }

        (_player as EntityPlayerLocal).inventory.holdingItemItemValue.UseTimes += EffectManager.GetValue(
            PassiveEffects.DegradationPerUse,
            (_player as EntityPlayerLocal).inventory.holdingItemItemValue,
            1f,
            _player,
            (Recipe)null,
            (_player as EntityPlayerLocal).inventory.holdingItem.ItemTags,
            true,
            true,
            true,
            true,
            1,
            true
        ) * (repairDifficulty + 1f);
        (_player as EntityPlayerLocal).inventory.HoldingItemHasChanged();

        playerUi.windowManager.Open("timer", true, false, true);
        XUiC_Timer childByType = playerUi.xui.GetChildByType<XUiC_Timer>();
        TimerEventData _eventData = new TimerEventData();
        _eventData.CloseEvent += this.EventData_CloseEvent;
        _eventData.Data = (object)new object[4]
        {
          (object) _cIdx,
          (object) _blockValue,
          (object) _blockPos,
          (object) _player
        };
        _eventData.Event += this.EventData_Event;
        _eventData.alternateTime = _player.rand.RandomRange(this.repairTime * this.repairChance, this.repairTime + 1f);
        _eventData.AlternateEvent += this.EventData_CloseEvent;
        childByType.SetTimer(this.repairTime, _eventData);
        return true;
    }

    private void EventData_CloseEvent(TimerEventData timerData)
    {
        object[] data = (object[])timerData.Data;
        Vector3i _blockPos = (Vector3i)data[2];
        EntityPlayerLocal entityPlayerLocal = data[3] as EntityPlayerLocal;
        GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttItemNeedsRepair", ""));
        this.ResetEventData(timerData);
    }

    private void EventData_Event(TimerEventData timerData)
    {
        World world = GameManager.Instance.World;
        object[] array = (object[])timerData.Data;
        int clrIdx = (int)array[0];
        BlockValue blockValue = (BlockValue)array[1];
        Vector3i vector3i = (Vector3i)array[2];
        BlockValue block = world.GetBlock(vector3i);
        EntityPlayerLocal entityPlayerLocal = array[3] as EntityPlayerLocal;
        LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
        ItemValue itemValue = ItemClass.GetItem(this.repairedItemValue, false);
        ItemStack itemStack = new ItemStack(itemValue, 1);
        if (!uiforPlayer.xui.PlayerInventory.AddItem(itemStack, true))
            uiforPlayer.xui.PlayerInventory.DropItem(itemStack);
        BlockValue downgradeBlock = BlockPlaceholderMap.Instance.Replace(BlockValue.Air, world.GetGameRandom(), vector3i.x, vector3i.z, false, QuestTags.none);
        downgradeBlock.rotation = block.rotation;
        downgradeBlock.meta = block.meta;
        world.SetBlockRPC(clrIdx, vector3i, downgradeBlock, Block.list[downgradeBlock.type].Density);
        entityPlayerLocal.Progression.AddLevelExp((int)repairExpValue, "_xpFromRepairBlock", Progression.XPTypes.Repairing, true);
        this.ResetEventData(timerData);
    }

    private void ResetEventData(TimerEventData timerData)
    {
        timerData.AlternateEvent -= this.EventData_CloseEvent;
        timerData.CloseEvent -= this.EventData_CloseEvent;
        timerData.Event -= this.EventData_Event;
    }
}